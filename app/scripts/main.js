/*global UaWebChallenge, $*/


window.UaWebChallenge = {
    Models: {},
    Collections: {},
    Views: {},
    Routers: {},
    init: function () {
        'use strict';
        console.log('Hello from Backbone!');      

        new UaWebChallenge.Routers.Router();
        Backbone.history.start();
    }
};

$(document).ready(function () {
    'use strict';
    UaWebChallenge.init();
});
