/*global UaWebChallenge, Backbone*/

UaWebChallenge.Collections = UaWebChallenge.Collections || {};

(function () {
    'use strict';

    UaWebChallenge.Collections.Category = Backbone.Collection.extend({

        model: UaWebChallenge.Models.Category

    });

})();
