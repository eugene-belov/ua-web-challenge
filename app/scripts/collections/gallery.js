/*global UaWebChallenge, Backbone*/

UaWebChallenge.Collections = UaWebChallenge.Collections || {};

(function () {
    'use strict';

    UaWebChallenge.Collections.Gallery = Backbone.Collection.extend({

        model: UaWebChallenge.Models.Photo

    });

})();
