/*global UaWebChallenge, Backbone, JST*/

UaWebChallenge.Views = UaWebChallenge.Views || {};

(function () {
    'use strict';

    UaWebChallenge.Views.Photos = Backbone.View.extend({
        template: JST['app/scripts/templates/photos.ejs'],
        el: '#app',
        events: {
          "click [role='next']" : "showNextPhoto",
          "click [role='prev']" : "showPrevPhoto",
          "click .like" : "likePhoto"
        },

        initialize: function () {
          this.count = 0;
          this.currentCategory = '';
          this.width = 0;
          this.height = 0;

          this.likesCollection = new UaWebChallenge.Collections.Gallery();
          this.allCollection = new UaWebChallenge.Collections.Gallery();

          this.categoryCollection = new UaWebChallenge.Collections.Category();
          this.categoriesView = new UaWebChallenge.Views.Categories({
            collection: this.categoryCollection
          });

          this.allCollection.on('reset', function(col, opts) {
            //col.remove(opts.previousModels)
            _.each(opts.previousModels, function(model) {
              model.trigger('remove');
              model.destroy();
            });
          });

          Backbone.Events.on("settings:change", function(data) {
            this.width = data.get('width');
            this.height = data.get('height');
          }.bind(this));
        },

        showAll: function(obj) {
          this.currentCategory = (!obj) ? this.currentCategory : obj.category;
          console.log("all", this.allCollection.models);

          this.allCollection.reset();
          this.count = 0;
          this.collection = this.allCollection;
          this.fillGalleryCollection();

          return this;
        },

        showLikes: function() {
          console.log("likes", this.likesCollection.models);

          this.count = 0;
          this.collection = this.likesCollection;

          return this;
        },

        fillGalleryCollection: function() {
          for(var i = 0; i < 10; i++) {
            this.allCollection.add({
              width: this.width,
              height: this.height,
              category: this.currentCategory,
              liked: false,
              number: i
            })
          }
        },

        showPrevPhoto: function() {
          (this.count > 0) ? this.count-- : this.count = 0;
          this.render();
        },

        showNextPhoto: function() {
          (this.count < this.collection.length -1) ? this.count++ : this.count = this.collection.length -1;
          this.render();
        },

        likePhoto: function (event) {
          this.likesCollection.add({
            width: this.width,
            height: this.height,
            category: this.currentCategory,
            liked: true,
            number: this.count
          });

          $(event.currentTarget).animate({opacity: 0}, 500, "linear", function() {
            $(event.currentTarget).hide();
            this.collection.at(this.count).set('liked', true)
          }.bind(this));
        },

        render: function () {
          //var loader = new Image();
          //loader.onload = function () { console.log('loader loaded - render view'); }
          //loader.src = 'http://lorempixel.com/400/200/sports/1/';

          this.$el.empty().append(this.template({
              width: this.width,
              height: this.height,
              number: this.count,
              category: this.collection.at(this.count).get('category'),
              likes: this.likesCollection.length > 0,
              isliked:  this.collection.at(this.count).get('liked'),
              img: 'http://lorempixel.com/'+ this.width +'/'+ this.height +'/'+ this.collection.at(this.count).get('category') +"/"+ this.collection.at(this.count).get('number')
          }));

          this.categoriesView.render(this.$el.find('#categories'));

          return this;
        }

    });

})();
