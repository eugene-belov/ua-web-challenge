/*global UaWebChallenge, Backbone, JST*/

UaWebChallenge.Views = UaWebChallenge.Views || {};

(function () {
    'use strict';

    UaWebChallenge.Views.Categories = Backbone.View.extend({
        template: JST['app/scripts/templates/categories.ejs'],
        events: {},

        initialize: function () {
            this.collection.add([
              new UaWebChallenge.Models.Category({title: 'Sports', link: 'sports'}),
              new UaWebChallenge.Models.Category({title: 'Food', link: 'food'}),
              new UaWebChallenge.Models.Category({title: 'Cats', link: 'cats'}),
              new UaWebChallenge.Models.Category({title: 'City', link: 'city'}),
              new UaWebChallenge.Models.Category({title: 'Transport', link: 'transport'})
            ])
        },

        render: function (node) {
            node.html(this.template({
              category: this.collection.toJSON()
            }));

            return this;
        }

    });

})();
