/*global UaWebChallenge, Backbone, JST*/

UaWebChallenge.Views = UaWebChallenge.Views || {};

(function () {
    'use strict';

    UaWebChallenge.Views.Settings = Backbone.View.extend({
        template: JST['app/scripts/templates/settings.ejs'],
        el: '#app',
        events: {
            "click .btn" : "saveSettings",
            "input [role='height']" : "validateNumbers",
            "input [role='width']" : "validateNumbers"
        },

        initialize: function () {
            this.settings = new UaWebChallenge.Models.Gallery({
                width: 200,
                height: 200
            });
            Backbone.Events.trigger("settings:change", this.settings);
        },

        validateNumbers: function(event) {
            var value = event.currentTarget.value.replace(/[^\d]+/g,'');
            event.currentTarget.value = (parseInt(value) > 1980) ? 1980 : value;
        },

        saveSettings: function (argument) {
          console.log("Save settings handler", this.$width.val(), this.$height.val());

            this.settings.set('width', this.$width.val());
            this.settings.set('height', this.$height.val());

            Backbone.Events.trigger("settings:change", this.settings);
        },

        render: function () {
            this.$el.html(this.template());

            this.$width = this.$el.find("input[role='width']");
            this.$height = this.$el.find("input[role='height']");

            this.$width.val(this.settings.get('width'));
            this.$height.val(this.settings.get('height'));

            return this;
        }

    });

})();
