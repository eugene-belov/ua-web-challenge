/*global UaWebChallenge, Backbone*/

UaWebChallenge.Models = UaWebChallenge.Models || {};

(function () {
    'use strict';

    UaWebChallenge.Models.Photo = Backbone.Model.extend({

        initialize: function() {
        },

        defaults: {
          width: 200,
          height: 200,
          category: 'sports',
          liked: false
        },

        validate: function(attrs, options) {
        },

        parse: function(response, options)  {
          return response;
        }
    });

})();
