/*global UaWebChallenge, Backbone*/

UaWebChallenge.Models = UaWebChallenge.Models || {};

(function () {
    'use strict';

    UaWebChallenge.Models.Gallery = Backbone.Model.extend({
        //categories: new UaWebChallenge.Collections.Category()

        defaults: {
            width: 0,
            height: 0
        }
    });

})();
