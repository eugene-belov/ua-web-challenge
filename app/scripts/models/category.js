/*global UaWebChallenge, Backbone*/

UaWebChallenge.Models = UaWebChallenge.Models || {};

(function () {
    'use strict';

    UaWebChallenge.Models.Category = Backbone.Model.extend({
        defaults: {
          title: '',
          link: '',
          photo: new UaWebChallenge.Models.Photo()
        }
    });
})();
