/*global UaWebChallenge, Backbone*/

UaWebChallenge.Routers = UaWebChallenge.Routers || {};

(function () {
    'use strict';

    UaWebChallenge.Routers.Router = Backbone.Router.extend({

      routes: {
        "photos": "photos",
        "photos/:category": "photosInCategory",
        "photos/filter/:flag": "filterPhotos",
        "settings": "settings",
        "*actions": "defaultRoute"
      },

      initialize: function() {
        this.photosView = new UaWebChallenge.Views.Photos();
        this.settingsView = new UaWebChallenge.Views.Settings();
      },

      photos: function(param) {
        this.photosView
            .showAll({ category: 'sports' })
            .render();
      },

      photosInCategory: function(param) {
        console.log("Category", param);
        this.photosView
            .showAll({ category: param })
            .render();
      },

      filterPhotos: function(param) {
        (param == 'all')
            ?
            this.photosView
                .showAll()
                .render()
            :
            this.photosView
                .showLikes()
                .render();
      },

      settings: function () {
        this.settingsView.render();
      },

      defaultRoute: function () {
        this.navigate("photos", {trigger: true, replace: true});
      }

    });

})();
